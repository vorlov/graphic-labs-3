import pytest
import pathlib

from pic.formats.factory import ImageFactory
from pic.base.image import ImageData
from pic.errors import BMPBitError, BMPImageValidationError

from . import create_image


GOOD_IMAGES_PATH = pathlib.Path('pic/tests/images/bmp/24_32')
BIT_ERROR_IMAGES_PATH = pathlib.Path('pic/tests/images/bmp/biterror')
FORMAT_ERROR_IMAGES_PATH = pathlib.Path('pic/tests/images/bmp/formaterror')
STASH_PATH = pathlib.Path('stash')
STASH_PATH.mkdir(exist_ok=True)


### READ ####
@pytest.fixture
def bmp_reader():
    return ImageFactory.get_reader('bmp')


@pytest.mark.parametrize('bmp_abs_path', [x for x in GOOD_IMAGES_PATH.glob('*.bmp') if x.is_file()])
def test_good_bmp_reader(bmp_abs_path, bmp_reader):
    image = bmp_reader.read(bmp_abs_path.as_posix())
    assert isinstance(image, ImageData)


@pytest.mark.parametrize('bmp_abs_path', [x for x in BIT_ERROR_IMAGES_PATH.glob('*.bmp') if x.is_file()])
def test_bit_error(bmp_abs_path, bmp_reader):
    with pytest.raises(BMPBitError):
        image = bmp_reader.read(bmp_abs_path.as_posix())
        assert isinstance(image, ImageData)


@pytest.mark.parametrize('bmp_abs_path', [x for x in FORMAT_ERROR_IMAGES_PATH.glob('*.*') if x.is_file()])
def test_format_error(bmp_abs_path, bmp_reader):
    with pytest.raises(BMPImageValidationError):
        image = bmp_reader.read(bmp_abs_path.as_posix())
        assert isinstance(image, ImageData)


#### WRITE ####
@pytest.fixture
def bmp_writer():
    return ImageFactory.get_writer('bmp')


@pytest.mark.parametrize('image', [
    create_image(8, 8),
    create_image(16, 16),
    create_image(32, 32),
    create_image(64, 64),
    create_image(128, 128)
])
def test_bmp_writer(image, bmp_writer, bmp_reader):
    bmp_writer.write(STASH_PATH / 'name.bmp', image)
    _image = bmp_reader.read(STASH_PATH / 'name.bmp')

    assert image == _image
