import pytest
import pathlib

from pic.formats.factory import ImageFactory
from pic.base.image import ImageData
from . import create_image, STASH_PATH

GOOD_IMAGES_PATH = pathlib.Path('pic/tests/images/ppm')


# TODO: write tests

@pytest.fixture
def ppm_reader():
    return ImageFactory.get_reader('ppm')


@pytest.mark.parametrize('ppm_abs_path', [x for x in GOOD_IMAGES_PATH.glob('*.ppm') if x.is_file()])
def test_ppm_reader(ppm_abs_path, ppm_reader):
    image = ppm_reader.read(ppm_abs_path.as_posix())

    assert isinstance(image, ImageData)


#### WRITE ####
@pytest.fixture
def ppm_writer():
    return ImageFactory.get_writer('ppm')


@pytest.mark.parametrize('image', [
    create_image(8, 8),
    create_image(16, 16),
    create_image(32, 32),
    create_image(64, 64),
    create_image(128, 128)
])
def test_ppm_writer(image, ppm_writer, ppm_reader):
    ppm_writer.write(STASH_PATH / 'name.ppm', image)
    _image = ppm_reader.read(STASH_PATH / 'name.ppm')

    assert image == _image
