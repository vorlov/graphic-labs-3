import pathlib
import random
import typing
from pic.base.image import ImageData

STASH_PATH = pathlib.Path('pic/tests/stash')
STASH_PATH.mkdir(exist_ok=True)


def create_image(width: int, height: int, bytes_per_pixel: typing.Optional[int] = 3) -> ImageData:
    return ImageData(width, height,
                     [[random.randint(0, 255) for j in range(width * bytes_per_pixel)] for i in range(height)])
