import pytest
import typing

from pic.errors import FormatIsNotSupportedForReadError, FormatIsNotSupportedForWriteError
from pic.formats.factory import ImageFactory
from pic.formats import *
from pic.formats.factory import _ReadFormats, _WriteFormats

AVAILABLE_READ_FORMATS = [
    ('bmp', BmpReader),
    ('ppm', PPMReader)
]

AVAILABLE_WRITE_FORMATS = [
    ('bmp', BmpWriter),
    ('ppm', PPMWriter)
]

NOT_AVAILABLE_READ_FORMATS = ['png', 'jpeg', 'gif', 'tiff', 'pdf']
NOT_AVAILABLE_WRITE_FORMATS = ['png', 'jpeg', 'gif', 'tiff', 'pdf']


@pytest.fixture
def image_factory():
    return ImageFactory


@pytest.mark.parametrize('read_format', AVAILABLE_READ_FORMATS)
def test_read_formats(read_format, image_factory):
    _format: _ReadFormats = read_format[0]
    _format_cls: typing = read_format[1]

    assert isinstance(ImageFactory.get_reader(_format), _format_cls)


@pytest.mark.parametrize('write_format', AVAILABLE_WRITE_FORMATS)
def test_write_formats(write_format, image_factory):
    _format: _ReadFormats = write_format[0]
    _format_cls: typing = write_format[1]

    assert isinstance(ImageFactory.get_writer(_format), _format_cls)


@pytest.mark.parametrize('read_format', NOT_AVAILABLE_READ_FORMATS)
def test_error_read_format(read_format, image_factory):
    assert read_format not in [x[0] for x in AVAILABLE_READ_FORMATS]

    with pytest.raises(FormatIsNotSupportedForReadError):
        image_factory.get_reader(read_format)


@pytest.mark.parametrize('write_format', NOT_AVAILABLE_WRITE_FORMATS)
def test_error_write_format(write_format, image_factory):
    assert write_format not in [x[0] for x in AVAILABLE_WRITE_FORMATS]

    with pytest.raises(FormatIsNotSupportedForWriteError):
        image_factory.get_writer(write_format)
