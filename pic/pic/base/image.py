import typing
from abc import ABC, abstractmethod
from dataclasses import dataclass

from .file import ReadFileMixin

TypePixelArray = typing.List[typing.List[int]]


@dataclass
class ImageData:
    width: int
    height: int
    pixel_array: TypePixelArray


class BaseReader(ReadFileMixin):
    @abstractmethod
    def read(self, path: str) -> ImageData: ...


class BaseWriter(ABC):
    @abstractmethod
    def write(self, path: str, image: ImageData): ...
