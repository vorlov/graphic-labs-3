from abc import abstractmethod, ABC

import typing


class ReadFileMixin(ABC):
    @staticmethod
    @abstractmethod
    def _read_file(path: str) -> typing.Any: ...

# class WriteFileMixin(ABC):
#     @staticmethod
#     @abstractmethod
#     def _write_file(path: str, content: bytes): ...
