import typing

from ..base.image import BaseReader, BaseWriter

from .bmp import BmpReader, BmpWriter
from .ppm import PPMReader, PPMWriter
from .jpeg import JPEGReader

from pic.errors import FormatIsNotSupportedForReadError, FormatIsNotSupportedForWriteError

_WriteFormats = typing.Literal[
    'bmp',
    'ppm'
]

_ReadFormats = typing.Literal[
    'bmp',
    'ppm',
    'jpeg',
    'jpg'
]


class ImageFactory:
    @staticmethod
    def get_writer(image_format: _WriteFormats) -> BaseWriter:
        if image_format == 'bmp':
            return BmpWriter()
        elif image_format == 'ppm':
            return PPMWriter()

        raise FormatIsNotSupportedForWriteError(image_format)

    @staticmethod
    def get_reader(image_format: _ReadFormats) -> BaseReader:
        if image_format == 'bmp':
            return BmpReader()
        elif image_format == 'ppm':
            return PPMReader()
        elif image_format == 'jpeg':
            return JPEGReader()
        elif image_format == 'jpg':
            return JPEGReader()

        raise FormatIsNotSupportedForReadError(image_format)
