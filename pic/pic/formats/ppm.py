import typing

from ..base.image import BaseReader, BaseWriter, ImageData

from pic.utils import perform_pixels
from pic.errors import PPMImageValidationError


class PPMReader(BaseReader):
    def read(self, path) -> ImageData:
        raw_ppm_data = self._read_file(path)

        width, height = raw_ppm_data[1], raw_ppm_data[2]
        max_pixel_value = raw_ppm_data[3]
        image = raw_ppm_data[4:] if max_pixel_value == 255 else self._scale_colors(raw_ppm_data[4:],
                                                                                   int(max_pixel_value))
        return ImageData(int(width), int(height), perform_pixels(image, int(width), int(height), bytes_per_pixel=3))

    def _read_file(self, path) -> typing.List[str]:
        with open(path, 'rb') as ppm_file:
            while True:
                line = ppm_file.readline()
                if line[0] != '#':
                    try:
                        filetype = line.decode()[:2]
                        break
                    except UnicodeDecodeError:
                        raise PPMImageValidationError(path)
        if filetype == "P6":
            return self._read_p6(path)

        elif filetype == "P3":
            return self._read_p3(path)

        else:
            raise PPMImageValidationError(path)

    def _read_p3(self, path) -> typing.List[str]:
        with open(path, 'r') as p3_file:
            _temp_lines = list(filter(self._validate_str_line,
                                      p3_file.read().split('\n')))

            header = _temp_lines[:3]
            header = ' '.join(list(map(lambda line: line.split('#')[0], header))).split(' ')
            pixels = ' '.join(_temp_lines[3:]).split()
            return header + pixels

    def _read_p6(self, path) -> typing.List[str]:
        header, header_end_idx = self._get_p6_header_last_idx(path)
        with open(path, 'rb') as p6_file:
            _temp_lines = list(filter(self._validate_byte_line,
                                      p6_file.read().split(b'\n')))
            pixels = list(map(lambda byte: str(int(byte)), b"\n".join(_temp_lines[header_end_idx:])))

            return header + pixels

    @staticmethod
    def _get_p6_header_last_idx(path: str):
        for_header = open(path, 'rb')
        header = []
        header_end_idx = 0
        temp_chunk = ''
        while True:
            token = for_header.read(1)
            try:
                decoded = token.decode()
                if decoded == "#":
                    header_end_idx += 1
                    for_header.readline()
                elif decoded == "\n":
                    header_end_idx += 1
                    header.append(temp_chunk)
                    temp_chunk = ''
                else:
                    temp_chunk += decoded
            except:
                break
        for_header.close()
        header = ' '.join(list(map(lambda line: line.split('#')[0], header))).split()
        return [header, header_end_idx]

    @staticmethod
    def _scale_colors(image: typing.List[str], max_value: int) -> typing.List[int]:
        return list(map(lambda byte: int((255 / int(max_value)) * int(byte)), image))

    @staticmethod
    def _validate_str_line(line: str) -> bool:
        return line != '' and line[0] != '#'

    @staticmethod
    def _validate_byte_line(line: bytes) -> bool:
        return list != b''


class PPMWriter(BaseWriter):
    def write(self, path: str, image: ImageData):
        with open(path, 'w') as file:
            file.write("P3\n")

            file.write(f'{image.width} {image.height}\n')
            file.write(f'{str(255)}\n\n')
            for line in image.pixel_array:
                for pixel in line:
                    file.write(f'{str(pixel)}\n')
