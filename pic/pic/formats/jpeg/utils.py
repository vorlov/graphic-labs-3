import math
import typing
import numpy as np


def zig_zag_index(k, n):
    if k >= n * (n + 1) // 2:
        i, j = zig_zag_index(n * n - 1 - k, n)
        return n - 1 - i, n - 1 - j

    i = int((math.sqrt(1 + 8 * k) - 1) / 2)
    j = k - i * (i + 1) // 2
    return (j, i - j) if i & 1 else (i - j, j)


def transpose(matrix: typing.List[typing.List[typing.Any]]) -> typing.List[typing.List[typing.Any]]:
    return list(map(list, zip(*matrix)))


def multiply(x, y) -> typing.List[typing.Any]:
    return np.matmul(x, y).tolist()


def round_matrix(m):
    return [[round(j) for j in i] for i in m]


def multiply_v_by_v(x, y):
    return [[j_x * j_y for j_x, j_y in zip(x[i], y[i])] for i in range(len(y))]


def divide_v_by_v(x, y):
    return [[j_x / j_y for j_x, j_y in zip(x[i], y[i])] for i in range(len(y))]


def limit_matrix(matrix, _min: int, _max: int):
    return [[max(min(255, j + 128), 0) for j in i] for i in matrix]


def split(array, nrows, ncols):
    """Split a matrix into sub-matrices."""

    r, h = array.shape
    return (array.reshape(h // nrows, nrows, -1, ncols)
            .swapaxes(1, 2)
            .reshape(-1, nrows, ncols))
