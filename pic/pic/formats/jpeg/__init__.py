import logging

from pic.log import wrap_logger, STREAM_HANDLER, FILE_HANDLER
from .reader import JPEGReader

wrap_logger('jpeg', logging.INFO, STREAM_HANDLER)

# WARNING!!!! COMMENT IT WHEN PRODUCTION
# wrap_logger('jpeg', logging.DEBUG, FILE_HANDLER)

__all__ = (JPEGReader,)
