__doc__ = '''
We already know:
    * RGB -> YCbCr
    * downsampling CbCr (4 pixels -> 1 pixel)
    * Y | Cb | Br : divide by 8x8. For each block we build DCT coef table
        * Get T (Transformation table). Matlab func: dctmtx(8)
        * DCT table = T * (Y | Cb | Cr) * T'
    * Getting lots of zeros
        * Create quantization table for luminance and chrominance (Y - luminance, CbCr - chrominance)
        * Multiply Q on DCT coef table (DCT<Y>(i;j) * Q[l] | DCT<Cb>(i;j) * Q[c] | DCT<Cr(i;j) * Q[c])
    * Huffman encoding
        * ZigZag each 8x8 block -> List<64>[4, 2, 3, 1, 0, ..., 0]
        * https://www.impulseadventure.com/photo/jpeg-huffman-coding.html
    * Writing Jpeg structure
'''

import numpy as np

from pic.base.image import BaseReader, ImageData
from pic.utils import perform_pixels

from .components import *
from .huffman import HuffmanTable
from .errors import JPEGComputingError, JPEGReadError

log = logging.getLogger('jpeg')


class JPEGReader(BaseReader):

    def read(self, path: str) -> ImageData:
        """
        Header -> 2 quant table -> 4 Huffman table -> Start of scan -> Raw image data(coded) -> EOI
        """
        try:
            decoded_data = self._read_file(path)
        except Exception as e:
            raise JPEGReadError(e)

        ycbcr_decoder = YCbCrDecoder(decoded_data)
        raw_rgb = ycbcr_decoder.decode()
        return ImageData(width=decoded_data.sof.width, height=decoded_data.sof.height,
                         pixel_array=perform_pixels(width=decoded_data.sof.width,
                                                    height=decoded_data.sof.height, raw_pixels=raw_rgb,
                                                    bytes_per_pixel=3))

    def _read_file(self, path: str) -> DecodedData:
        with open(path, 'rb') as file:
            decoded_data = self._decode(file)
        return decoded_data

    # --- Decoding image ---
    def _decode(self, file: typing.BinaryIO):
        header = None
        q_tables = []
        sof = None
        h_tables = []
        log.info('Reading image...')

        while True:
            hex_val = file.read(2).hex()

            if M.equal(M.JFIF_SOI, hex_val):
                log.debug(f'Key: {hex_val.upper()}, SOI')

            elif M.equal(M.JFIF_APP0, hex_val):
                log.debug(f'Key: {hex_val.upper()}, APP_HEADER')

                length = int(file.read(2).hex(), 16)
                data = file.read(length - 2)
                header = self._decode_header(data)

            elif M.equal(M.JFIF_DQT, hex_val):
                log.debug(f'Key: {hex_val.upper()}, Q_TABLE')
                length = int(file.read(2).hex(), 16)
                data = file.read(length - 2)
                q_table = self._decode_q_table(data)
                q_tables.append(q_table)

            elif M.equal(M.JFIF_SOF0, hex_val):
                log.debug(f'Key: {hex_val.upper()}, SOF0')

                length = int(file.read(2).hex(), 16)
                data = file.read(length - 2)
                sof = self._decode_sof(data)

            elif M.equal(M.JFIF_DHT, hex_val):
                log.debug(f'Key: {hex_val.upper()}, HUFFMAN')

                length = int(file.read(2).hex(), 16)
                data = file.read(length - 2)
                table_data = self._decode_huffman_table(data)
                h_tables.append(table_data)

            elif M.equal(M.JFIF_SOS, hex_val):
                log.debug(f'Key: {hex_val.upper()}, SOS')

                length = int(file.read(2).hex(), 16)
                data = file.read(length - 2)
                sos = self._decode_sos(data)
                raw_image_data = self._decode_image_data(file)
                break
            else:
                file.read(int(file.read(2).hex(), 16) - 2)

        return DecodedData(
            header,
            q_tables,
            sof,
            h_tables,
            sos,
            raw_image_data
        )

    @staticmethod
    def _decode_header(data: bytes):
        identifier = str(data[:4].decode())
        version = str(data[5]) + '.' + str(data[6])
        units = str(data[7])
        density = [int(data[8:10].hex(), 16), int(data[10:12].hex(), 16)]
        thumbnail = [int(data[12]), int(data[13])]
        return Header(
            identifier,
            version,
            units,
            density,
            thumbnail
        )

    @staticmethod
    def _decode_q_table(data: bytes):
        destination = 'luminance' if int(data[0]) == 0 else 'chrominance'
        return QTable(
            destination,
            np.array([x for x in data[1:]]).reshape((8, 8)).tolist()
        )

    @staticmethod
    def _decode_sof(data: bytes):
        precision = int(data[:1].hex(), 16)
        height = int(data[1:3].hex(), 16)
        width = int(data[3:5].hex(), 16)
        components_length = int(data[5])
        components_data = []
        for i in range(components_length):
            component_data = {'id': data[6 + (i * 3):7 + (i * 3)].hex(),
                              'factor': data[6 + (i * 3) + 1:7 + (i * 3) + 1].hex(),
                              'table': data[6 + (i * 3) + 2:7 + (i * 3) + 2].hex()}
            components_data.append(component_data)
        return SOF(
            precision,
            height,
            width,
            components_length,
            components_data
        )

    @staticmethod
    def _decode_huffman_table(data: bytes):
        table_class = data[0]
        amounts = [x for x in data[1:17]]
        values = list([x for x in data[17:]])
        hft = HuffmanTable()
        hft.get_huffman_bits(amounts, values)
        return HTable(
            table_class,
            hft.get_map()
        )

    @staticmethod
    def _decode_sos(data: bytes):
        components_count = data[0]
        components = []
        for i in range(components_count):
            components.append({'selector': data[i * 2], 'dc_ac': data[(i * 2) + 1]})
        spectral_sel = data[(components_count * 2) + 2]
        suc_approx = data[(components_count * 2) + 3]
        return SOS(
            components,
            spectral_sel,
            suc_approx
        )

    @staticmethod
    def _decode_image_data(file: typing.BinaryIO):
        data = file.read()
        data = [x for x in data[:]]
        end_marker = [data.pop(), data.pop()]
        end_marker.reverse()
        if bytearray(end_marker).hex() == 'ffd9':
            return data
        else:
            JPEGReadError('Image data is incorrect')
