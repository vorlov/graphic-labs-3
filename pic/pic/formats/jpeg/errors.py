class JPEGComputingError(Exception):
    def __init__(self, message):
        super().__init__(message)


class JPEGReadError(Exception):
    def __init__(self, message):
        super().__init__(message)


class JPEGHuffmanError(Exception):
    def __init__(self, message):
        super().__init__(message)
