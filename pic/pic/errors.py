class BaseImageValidationError(Exception):
    def __init__(self, path: str, image_format: str):
        super().__init__(f'File {path} is not {image_format} image format')


class BMPImageValidationError(BaseImageValidationError):
    def __init__(self, path):
        super().__init__(path, 'BMP')


class BMPBitError(Exception):
    def __init__(self, bit_count: int):
        super().__init__(f'Converter does not support {bit_count}-bit bitmap')


class PPMImageValidationError(BaseImageValidationError):
    def __init__(self, path):
        super().__init__(path, 'PPM')


class JPGImageValidationError(BaseImageValidationError):
    def __init__(self, path):
        super().__init__(path, 'JPG')


class FormatIsNotSupportedForWriteError(Exception):
    def __init__(self, image_format):
        super().__init__(f'.{image_format} is not supported for writing')


class FormatIsNotSupportedForReadError(Exception):
    def __init__(self, image_format):
        super().__init__(f'.{image_format} is not supported for reading')
