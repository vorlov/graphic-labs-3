import random
import typing

import numpy as np
from pic.base.image import TypePixelArray, ImageData


def perform_pixels(raw_pixels: typing.Any, width: int, height: int, bytes_per_pixel: int) -> TypePixelArray:
    raw_pixels = np.array(raw_pixels)
    return raw_pixels.reshape((height, width * bytes_per_pixel)).tolist()


def create_image(width: int, height: int, bytes_per_pixel: typing.Optional[int] = 3) -> ImageData:
    return ImageData(width, height,
                     [[random.randint(0, 255) for j in range(width * bytes_per_pixel)] for i in range(height)])


