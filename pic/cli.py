import argparse
import pathlib

from pic import ImageFactory


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--source", help='Input file for conversion (file path)')
    parser.add_argument("--goal-format", help='Output format for conversion (eg bpm or ppm)', default='.')
    parser.add_argument("--output", help='Path to the result')
    args = parser.parse_args()

    source = pathlib.Path(args.source)
    input_format = source.suffix[1:]

    r_image = ImageFactory.get_reader(input_format)
    w_image = ImageFactory.get_writer(args.goal_format)

    image = r_image.read(source)
    w_image.write(args.output or source.stem + '.' + args.goal_format, image)


if __name__ == '__main__':
    main()
