from pic import ImageFactory

ppm_reader = ImageFactory.get_reader('ppm')
ppm_writer = ImageFactory.get_writer('ppm')

bmp_reader = ImageFactory.get_reader('bmp')
bmp_writer = ImageFactory.get_writer('bmp')

PPM_PATH = ''
BMP_PATH = ''

# PPM -> BMP
ppm_image = ppm_reader.read(PPM_PATH)
bmp_writer.write(BMP_PATH, ppm_image)


# BMP -> PPM
bmp_image = bmp_reader.read(BMP_PATH)
ppm_writer.write(PPM_PATH, bmp_image)
